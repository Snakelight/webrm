import React from 'react'

export default props => {        
    return(
        <div style={{borderBottom:'1px solid #DEDEDE',backgroundColor:'#B8DCEE'}}>
            <h3>Car name: {props.name}</h3>
            <p>Year: <strong>{props.year}</strong></p>
            <button onClick={props.onChangeTitle}>Click</button>
        </div>
    )
}