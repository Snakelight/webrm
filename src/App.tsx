//import * as React from 'react'
import React, { Component } from "react";
import Car from "./components/car";
import "./App.css";
import {
  Column,
  SearchField,
  TitleBar,
  Container,
  Button,
  Menu,
  MenuItem,
  Panel,
  TreeList,
  Grid,
  SegmentedButton
} from "@sencha/ext-modern";
import data from "./store/data";

import model from "./store/file.json";

//import json from './store/file.json'

declare var Ext: any;

Ext.require(
  "Ext.panel.Resizable"
  /*"Ext.grid.plugin.SummaryRow",
  "Ext.data.summary.Average",
  "Ext.data.summary.Max"*/
);

// Enable responsiveConfig app-wide. You can remove this if you don't plan to build a responsive UI.

export default class App extends Component {
  store = Ext.create("Ext.data.TreeStore", {
    rootVisible: true,
    root: data
  });

  state = {
    cars: [
      {
        name: "Skoda",
        year: 2013
      },
      {
        name: "VW",
        year: 2018
      },
      {
        name: "Audi",
        year: 2011
      }
    ],
    pageTitle: 1,
    micro: false,
    width: undefined
  };

  //const json = require('./file.json');
  //let j2son = model;

  storeGrid = new Ext.data.Store({
    //model:'data1',
    data: model.data.data1,
    autoLoad: false,
    pageSize: 0
    /*proxy: {
      type: 'ajax',      
      //url: './src/store/file.json'
      url: 'resources/mini.json'
    } */
  });

  /*storeGrid = new Ext.data.Store({
    //model:'',
    data : [
        {firstName: 'Peter',   lastName: 'Venkman'},
        {firstName: 'Egon',    lastName: 'Spengler'},
        {firstName: 'Ray',     lastName: 'Stantz'},
        {firstName: 'Winston', lastName: 'Zeddemore'}
    ],
    autoLoad: true,
});*/

  changeTitleHandler = value => {
    this.setState({
      pageTitle: value
    });
  };

  toggleMicro = (button, micro) => {
    //var json = require('./store/mini.json');
    //console.log(this.storeGrid);
    //console.log(model);
    //console.log(json);
    this.setState({
      micro,
      width: micro ? 56 : undefined
    });
  };

  render() {
    const { micro, width } = this.state;

    //console.log(this.storeGrid);
    //const json = require('./store/file.json');

    return (
      <Container layout="vbox">
        <Panel layout="fit" flex={1}>
          <Panel docked="top">
            <TitleBar
              title="WebRM"
              docked="top"
              platformConfig={{
                phone: {
                  titleAlign: "center"
                }
              }}
            >
              <SegmentedButton allowMultiple>
                <Button
                  align="left"
                  iconCls="x-fa fa-bars"
                  pressed={micro}
                  onPressedChange={this.toggleMicro}
                  arrow={false}
                >
                  {Ext.os.is.Phone && (
                    <Menu>
                      <MenuItem text="Почта" iconCls="x-fa fa-inbox" />
                      <MenuItem text="Профиль" iconCls="x-fa fa-user" />
                    </Menu>
                  )}
                </Button>
              </SegmentedButton>

              {!Ext.os.is.Phone && (
                <Button align="right" iconCls="x-fa fa-inbox" text="Почта" />
              )}
              {!Ext.os.is.Phone && (
                <Button align="right" iconCls="x-fa fa-user" text="Профиль" />
              )}
              {!Ext.os.is.Phone && (
                <SearchField
                  align="right"
                  ui="alt"
                  placeholder="Поиск"
                  margin="0 10"
                />
              )}

              <Button align="right" iconCls="x-fa fa-ellipsis-v" arrow={false}>
                <Menu>
                  <MenuItem text="Настройки" iconCls="x-fa fa-cog" />
                  <MenuItem text="Помощь" iconCls="x-fa fa-question-circle" />
                </Menu>
              </Button>
            </TitleBar>

            {Ext.os.is.Phone && (
              <SearchField ui="faded" placeholder="Search" margin="20" />
            )}
          </Panel>
          <Panel
            docked="left"
            shadow={!Ext.os.is.Phone}
            scrollable
            width={width}
            platformConfig={{
              "!phone": {
                width: 250
              }
            }}
          >
            <TreeList
              ref="tree"
              width={width}
              expanderOnly={false}
              store={this.store}
              micro={micro}
              expanderFirst={true}
              ui="nav"
            />
          </Panel>
          <Panel>
            <Grid
              store={this.storeGrid}
              scrollable
              rowNumbers
              height="100%"
              plugins={{
                gridsummaryrow: true
              }}
            >
              <Column text="SHOP_SIGN" dataIndex="SHOP_SIGN" summaryRenderer={this.summarizeShop}/>
              <Column
                text="SPID_OR_GUID"
                dataIndex="SPID_OR_GUID"
                
              />
              <Column text="PP_RETAIL" dataIndex="PP_RETAIL"/>
              <Column text="ID_GOOD" dataIndex="ID_GOOD"/>
              <Column
                text="GOOD_NAME_SPB"
                dataIndex="GOOD_NAME_SPB"
                
              />
              <Column text="BONUS" dataIndex="BONUS"/>
              <Column
                text="ID_HD_PRICE_SHOP"
                dataIndex="ID_HD_PRICE_SHOP"
                
              />
              <Column text="PACKAGING" dataIndex="PACKAGING"/>
              <Column text="PRICE_BASE" dataIndex="PRICE_BASE"/>
              <Column text="BARCODE" dataIndex="BARCODE"/>
              <Column text="PRICE" dataIndex="PRICE"/>
              <Column
                text="ID_GROUP_TOP"
                dataIndex="ID_GROUP_TOP"
                
              />
              <Column text="ID_GROUP" dataIndex="ID_GROUP"/>
              <Column text="QTY" dataIndex="QTY"/>
              <Column
                text="ID_GROUP_CATEGORY"
                dataIndex="ID_GROUP_CATEGORY"
                
              />
              <Column text="CATEGORY" dataIndex="CATEGORY"/>
              <Column text="ID_POST" dataIndex="ID_POST"/>
              <Column text="GROUP1" dataIndex="GROUP1"/>
              <Column text="ID_VTM" dataIndex="ID_VTM"/>
              <Column text="GROUP2" dataIndex="GROUP2"/>
              <Column
                text="GOOD_NUMBER_MSK"
                dataIndex="GOOD_NUMBER_MSK"
                
              />
              <Column text="NAME_VTM" dataIndex="NAME_VTM"/>
              <Column text="NAME_POST" dataIndex="NAME_POST"/>
              <Column text="DEPART" dataIndex="DEPART"/>
              <Column
                text="PRICE_LABEL_TXT"
                dataIndex="PRICE_LABEL_TXT"
                
              />
              <Column
                text="DISCOUNT_PERCENT_ADV"
                dataIndex="DISCOUNT_PERCENT_ADV"
                
              />
              <Column
                text="DISCOUNT_PERCENT_ADV_SET"
                dataIndex="DISCOUNT_PERCENT_ADV_SET"
                
              />
              <Column
                text="NAME_ADV_TYPES_HIGHLIGHTNING"
                dataIndex="NAME_ADV_TYPES_HIGHLIGHTNING"
                
              />
              <Column text="NAME_BRAND" dataIndex="NAME_BRAND"/>
              <Column
                text="STOPPER_TYPE_PRINT"
                dataIndex="STOPPER_TYPE_PRINT"
                
              />
              <Column text="ID_BRAND" dataIndex="ID_BRAND"/>
            </Grid>
          </Panel>
        </Panel>
      </Container>
      // <div>
      //   <p>{this.state.pageTitle}</p>
      //   <button onClick={this.changeTitleHandler.bind(this, 'qwerty')}>Change title</button>

      //   {this.state.cars.map((car, index) => {
      //     return (
      //       <Car
      //         key={index}
      //         name={car.name}
      //         year={car.year}
      //         onChangeTitle={this.changeTitleHandler.bind(this, car.name)}
      //       />
      //     )
      //   })}
      // </div>
    );
  }
  summarizeShop = (grid, context) => "Строк: " + context.records.length;
}
